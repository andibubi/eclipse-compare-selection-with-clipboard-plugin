// Copyright 2020 Andreas Bubolz
// This code is licensed under MIT license (see LICENSE.txt for details)
package de.andreasbubolz.compareswcplugin;

import java.awt.HeadlessException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.eclipse.compare.CompareConfiguration;
import org.eclipse.compare.CompareEditorInput;
import org.eclipse.compare.IEncodedStreamContentAccessor;
import org.eclipse.compare.ITypedElement;
import org.eclipse.compare.internal.CompareDialog;
import org.eclipse.compare.internal.CompareUIPlugin;
import org.eclipse.compare.internal.Utilities;
import org.eclipse.compare.structuremergeviewer.DiffNode;
import org.eclipse.compare.structuremergeviewer.Differencer;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

public class CompareSWCHandler extends AbstractHandler {
	private CompareEditorInput input;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			ĉompare(event);
		} catch (CoreException | IOException | HeadlessException | UnsupportedFlavorException e) {
			throw new ExecutionException("Exception occured", e); //$NON-NLS-1$
		}
		return null;
	}

	private static class ComparisonInput implements ITypedElement, IEncodedStreamContentAccessor {
		static final String UTF_16 = "UTF-16"; //$NON-NLS-1$
		String content;

		ComparisonInput(String content) {
			this.content = content;
		}

		@Override
		public Image getImage() {
			return null;
		}

		@Override
		public String getName() {
			return null;
		}

		@Override
		public String getType() {
			return null;
		}

		@Override
		public InputStream getContents() {
			return new ByteArrayInputStream(Utilities.getBytes(content, UTF_16));
		}

		@Override
		public String getCharset() {
			return UTF_16;
		}
	}

	void ĉompare(ExecutionEvent event)
			throws CoreException, IOException, HeadlessException, UnsupportedFlavorException {

		IEditorPart editor = HandlerUtil.getActiveEditor(event);
		TextSelection selection = (TextSelection) editor.getEditorSite().getSelectionProvider().getSelection();

		// zumindest CompilationUnitEditor$AdaptedSourceViewer .. ruft StyledText.copy
		// und dies entfernt "\r"s.
		String left = selection.getText().replaceAll("\r?\n", "\n");
		String right = getClipboardContents(editor.getSite().getShell()).replaceAll("\r?\n", "\n");

		CompareConfiguration compareConfiguration = new CompareConfiguration();
		compareConfiguration.setLeftEditable(false);
		compareConfiguration.setLeftLabel("Selection");
		compareConfiguration.setRightEditable(false);
		compareConfiguration.setRightLabel("Clipboard");
		compareConfiguration.setProperty(CompareConfiguration.IGNORE_WHITESPACE, true);
		input = new CompareEditorInput(compareConfiguration) {
			@Override
			protected Object prepareInput(IProgressMonitor pm) {
				return new DiffNode(null, Differencer.CHANGE, null, new ComparisonInput(left),
						new ComparisonInput(right));
			}
		};
		openCompareDialog(input);
	}

	public static void openCompareDialog(CompareEditorInput input) {
		CompareUIPlugin plugin = CompareUIPlugin.getDefault();
		if (plugin != null) {
			if (plugin.compareResultOK(input, null)) {
				final CompareEditorInput input1 = input;
				Runnable runnable = () -> {
					CompareDialog dialog = new CompareDialog(
							PlatformUI.getWorkbench().getModalDialogShellProvider().getShell(), input1);
					dialog.open();
				};
				if (Display.getCurrent() == null)
					Display.getDefault().syncExec(runnable);
				else
					runnable.run();
			}
		}
	}

	private static final Transfer TEXT_TRANSFER = TextTransfer.getInstance();

	private String getClipboardContents(Shell shell) {
		String result = "";
		Clipboard clipboard = new Clipboard(shell.getDisplay());// Toolkit.getDefaultToolkit().getSystemClipboard();
		for (final TransferData transferData : clipboard.getAvailableTypes()) {
			if (TEXT_TRANSFER.isSupportedType(transferData)) {
				result = (String) clipboard.getContents(TEXT_TRANSFER);
			}
		}
		return result;
	}
}