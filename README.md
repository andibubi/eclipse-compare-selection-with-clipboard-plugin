# eclipse-compare-selection-with-clipboard-plugin

This is an eclipse plugin that allows comparing a text selected in an open editor with the content of the clipboard. 

In the editor right click and select **Compare With>Clipboard**.

For now this is an only rudimentary implementation.
Todos (at least): 
- Activation depending from selection 
- Maximum and minimum versions in MANIFEST.MF 